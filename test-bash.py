from datetime import datetime
from airflow.decorators import dag, task
from kubernetes.client import models as k8s
from airflow.models import Variable
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator

@dag(
    description='esto es una demo',
    schedule_interval='0 12 * * *',
    start_date=datetime(2024, 4, 16),
    catchup=False,
    tags=['test', 'dag'],
)
def test_bash():

    env_vars={
        "POSTGRES_USERNAME": Variable.get("POSTGRES_USERNAME"),
        "POSTGRES_PASSWORD": Variable.get("POSTGRES_PASSWORD"),
        "POSTGRES_DATABASE": Variable.get("POSTGRES_DATABASE"),
        "POSTGRES_HOST": Variable.get("POSTGRES_HOST"),
        "POSTGRES_PORT": Variable.get("POSTGRES_PORT"),
        "TRUE_CONNECTOR_EDGE_IP": Variable.get("CONNECTOR_EDGE_IP"),
        "TRUE_CONNECTOR_EDGE_PORT": Variable.get("IDS_EXTERNAL_ECC_IDS_PORT"),
        "TRUE_CONNECTOR_CLOUD_IP": Variable.get("CONNECTOR_CLOUD_IP"),
        "TRUE_CONNECTOR_CLOUD_PORT": Variable.get("IDS_PROXY_PORT"),
        "MLFLOW_ENDPOINT": Variable.get("MLFLOW_ENDPOINT"),
        "MLFLOW_TRACKING_USERNAME": Variable.get("MLFLOW_TRACKING_USERNAME"),
        "MLFLOW_TRACKING_PASSWORD": Variable.get("MLFLOW_TRACKING_PASSWORD")
    }

    volume_mount = k8s.V1VolumeMount(
        name="dag-dependencies", mount_path="/git"
    )

    init_container_volume_mounts = [
        k8s.V1VolumeMount(mount_path="/git", name="dag-dependencies")
    ]

    volume = k8s.V1Volume(name="dag-dependencies", empty_dir=k8s.V1EmptyDirVolumeSource())

    init_container = k8s.V1Container(
        name="git-clone",
        image="alpine/git:latest",
        command=["sh", "-c", "mkdir -p /git && cd /git && git clone -b main --single-branch https://gitlab.com/mikel_fernandez/test-platform.git"],
        volume_mounts=init_container_volume_mounts
    )

    @task.kubernetes(
        image='clarusproject/dag-image:1.0.0-slim',
        name='read',
        task_id='read',
        namespace='airflow',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        do_xcom_push=True,
        env_vars=env_vars
    )
    def read_task():
        path = '/git/test-platform/docker/'
    
        return path

    @task.kubernetes(
        task_id='build_push_image',
        name='build',
        namespace='airflow',
        image='mfernandezlabastida/kaniko:1.0',
        image_pull_policy='Always',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        env_vars=env_vars,
        do_xcom_push=True,
    )
    def push_task(path):
        # import subprocess
        import logging
        from kaniko import Kaniko, KanikoSnapshotMode

        logging.warning("Building and pushing image")
        kaniko = Kaniko()
        # kaniko.dockerfile = '/git/test-platform/docker/Dockerfile'
        kaniko.build(
            dockerfile=f'{path}/Dockerfile',
            context=path,
            # docker_registry_uri='http://registry-docker-registry.registry.svc.cluster.local:5000/',
            destination='registry.registry.svc.cluster.local:5000/mfernandezlabastida/image:1.2',
            snapshot_mode=KanikoSnapshotMode.full,
        )
        

        # subprocess.run(["/kaniko/executor", "--dockerfile=/git/test-platform/docker/Dockerfile", "--context=/git/test-platform/docker", "--destination=registry.registry.svc.cluster.local:5000/mfernandezlabastida/image:1.1"])

        # path = '/git/test-platform/docker/'
    
        # return path

    # build_push_image_result = KubernetesPodOperator(
    #     task_id='build_push_image',
    #     name='build',
    #     namespace='airflow',
    #     image='gcr.io/kaniko-project/executor:latest',
    #     image_pull_policy='Always',
    #     init_containers=[init_container],
    #     volumes=[volume],
    #     volume_mounts=[volume_mount],
    #     # cmds=["dockerd-entrypoint.sh"],
    #     arguments=["--dockerfile={}".format(read_task_result), "--context=/git/test-platform/docker", "--destination=registry.registry.svc.cluster.local:5000/mfernandezlabastida/image:1.0"],
    # )

    # build_push_image_result = build_push_image('docker')
    read_task_result = read_task()
    push_task_result = push_task(read_task_result)

    # Define the order of the pipeline
    read_task_result >> push_task_result
# Call the DAG
test_bash()