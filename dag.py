from datetime import datetime
from airflow.decorators import dag, task
from kubernetes.client import models as k8s
from airflow.models import Variable

@dag(
    description='esto es una demo',
    schedule_interval='0 12 * * *', 
    start_date=datetime(2024, 4, 16),
    catchup=False,
    tags=['test', 'dag'],
)
def test_platform_dag():

    env_vars={
        "POSTGRES_USERNAME": Variable.get("POSTGRES_USERNAME"),
        "POSTGRES_PASSWORD": Variable.get("POSTGRES_PASSWORD"),
        "POSTGRES_DATABASE": Variable.get("POSTGRES_DATABASE"),
        "POSTGRES_HOST": Variable.get("POSTGRES_HOST"),
        "POSTGRES_PORT": Variable.get("POSTGRES_PORT"),
        "TRUE_CONNECTOR_EDGE_IP": Variable.get("CONNECTOR_EDGE_IP"),
        "TRUE_CONNECTOR_EDGE_PORT": Variable.get("IDS_EXTERNAL_ECC_IDS_PORT"),
        "TRUE_CONNECTOR_CLOUD_IP": Variable.get("CONNECTOR_CLOUD_IP"),
        "TRUE_CONNECTOR_CLOUD_PORT": Variable.get("IDS_PROXY_PORT"),
        "MLFLOW_ENDPOINT": Variable.get("MLFLOW_ENDPOINT"),
        "MLFLOW_TRACKING_USERNAME": Variable.get("MLFLOW_TRACKING_USERNAME"),
        "MLFLOW_TRACKING_PASSWORD": Variable.get("MLFLOW_TRACKING_PASSWORD")
    }

    volume_mount = k8s.V1VolumeMount(
        name="dag-dependencies", mount_path="/git"
    )

    init_container_volume_mounts = [
        k8s.V1VolumeMount(mount_path="/git", name="dag-dependencies")
    ]

    volume = k8s.V1Volume(name="dag-dependencies", empty_dir=k8s.V1EmptyDirVolumeSource())

    init_container = k8s.V1Container(
        name="git-clone",
        image="alpine/git:latest",
        command=["sh", "-c", "mkdir -p /git && cd /git && git clone -b main --single-branch https://gitlab.com/mikel_fernandez/test-platform.git"],
        volume_mounts=init_container_volume_mounts
    )

    @task.kubernetes(
        image='clarusproject/dag-image:1.0.0-slim',
        name='read',
        task_id='read',
        namespace='airflow',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        do_xcom_push=True,
        env_vars=env_vars
    )
    def read_task():
        import sys
        import redis
        import uuid
        import pickle
    
        sys.path.insert(1, '/git/test-platform/src/redwine')
        from Data.read_data import read_data
        
        redis_client = redis.StrictRedis(
            host='redis-headless.redis.svc.cluster.local',
            port=6379,
            password='pass'
        )
    
        df = read_data()
        
        df_id = str(uuid.uuid4())
        redis_client.set(df_id, pickle.dumps(df))
    
        return df_id
    
    @task.kubernetes(
        image='clarusproject/dag-image:1.0.0-slim',
        name='process',
        task_id='process',
        namespace='airflow',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        do_xcom_push=True,
        env_vars=env_vars
    )
    def process_task(df_id):
        import sys
        import redis
        import uuid
        import pickle
    
        sys.path.insert(1, '/git/test-platform/src/redwine')
        from Process.data_processing import data_processing
        
        redis_client = redis.StrictRedis(
            host='redis-headless.redis.svc.cluster.local',
            port=6379,
            password='pass'
        )
        df = pickle.loads(redis_client.get(df_id))
    
        dp = data_processing(df)
        
        dp_id = str(uuid.uuid4())
        redis_client.set(dp_id, pickle.dumps(dp))
    
        return dp_id
    
    @task.kubernetes(
        image='clarusproject/dag-image:1.0.0-slim',
        name='SVC',
        task_id='SVC',
        namespace='airflow',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        do_xcom_push=True,
        node_selector={"kubernetes.io/hostname": "clarus-trainning.ipd.ikerlan.es"},
        env_vars=env_vars
    )
    def SVC_task(dp_id):
        import sys
        import redis
        import uuid
        import pickle
    
        sys.path.insert(1, '/git/test-platform/src/redwine')
        from Models.SVC_model_training import svc_model_training
        
        redis_client = redis.StrictRedis(
            host='redis-headless.redis.svc.cluster.local',
            port=6379,
            password='pass'
        )
        dp = pickle.loads(redis_client.get(dp_id))
    
        return svc_model_training(dp)
        
        
    @task.kubernetes(
        image='clarusproject/dag-image:1.0.0-slim',
        name='Elastic',
        task_id='Elastic',
        namespace='airflow',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        do_xcom_push=True,
        node_selector={"kubernetes.io/hostname": "clarus-trainning.ipd.ikerlan.es"},
        env_vars=env_vars
    )
    def Elastic_task(dp_id):
        import sys
        import redis
        import uuid
        import pickle
    
        sys.path.insert(1, '/git/test-platform/src/redwine')
        from Models.ElasticNet_model_training import elasticNet_model_training
        
        redis_client = redis.StrictRedis(
            host='redis-headless.redis.svc.cluster.local',
            port=6379,
            password='pass'
        )
        dp = pickle.loads(redis_client.get(dp_id))
    
        return elasticNet_model_training(dp)
        
        
    @task.kubernetes(
        image='clarusproject/dag-image:1.0.0-slim',
        name='redis_clean',
        task_id='redis_clean',
        namespace='airflow',
        init_containers=[init_container],
        volumes=[volume],
        volume_mounts=[volume_mount],
        do_xcom_push=True,
        env_vars=env_vars
    )
    def redis_clean_task(ids):
        import redis
    
        redis_client = redis.StrictRedis(
            host='redis-headless.redis.svc.cluster.local',
            port=6379,
            password='pass'
        )
    
        redis_client.delete(*ids)
    
    read_result = read_task()
    process_result = process_task(read_result)
    SVC_result = SVC_task(process_result)
    Elastic_result = Elastic_task(process_result)
    redis_task_result = redis_clean_task([read_result, process_result])
    
    # Define the order of the pipeline
    read_result >> process_result >> [SVC_result, Elastic_result] >> redis_task_result
# Call the DAG 
test_platform_dag()